import { Route, Routes } from "react-router-dom";

import Home from "@/pages/home";
import Collection from "@/pages/collection";

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/:addr" element={<Collection />} />
    </Routes>
  );
}

export default App;
