import React from "react";

import "./styles.css";

import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";

import { doSearchCollection } from "@/redux/actions/collections";
import { useDispatch } from "react-redux";

import SearchInput from "./internal/searchInput";
import SearchButton from "./internal/searchButton";
import SearchActions from "./internal/searchActions";

type Props = {
  address?: string;
};

function SearchBar(props: Props) {
  const { address } = props;

  const dispatch = useDispatch();

  const submittedAddr = React.useRef<string | undefined>(address);

  const { register, handleSubmit, getValues, watch } = useForm();
  watch("address");
  const addressValue = getValues("address") || address;

  const navigate = useNavigate();

  const [isSubmitting, setIsSubmitting] = React.useState(false);

  function handleSearch(formValues: any) {
    setIsSubmitting(true);

    const { address } = formValues;

    dispatch(doSearchCollection({ address }) as unknown as any)
      .then(() => navigate(`/${address}`))
      .catch(() => {});

    submittedAddr.current = address;
  }

  React.useEffect(() => {
    if (submittedAddr.current && submittedAddr.current !== addressValue) {
      submittedAddr.current = undefined;
    }
  }, [addressValue]);

  return (
    <>
      <div className="search">
        <form className="search__box" onSubmit={handleSubmit(handleSearch)}>
          <SearchInput {...register("address", { value: addressValue })} />
          <SearchButton />
        </form>
      </div>

      <SearchActions
        address={addressValue}
        didSubmit={submittedAddr.current === addressValue}
        isSubmitting={isSubmitting}
        setIsSubmitting={setIsSubmitting}
      />
    </>
  );
}

export default SearchBar;
