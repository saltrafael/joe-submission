import React from "react";
import "./styles.css";

const SearchInput = React.forwardRef((props: any, ref: any) => {
  return (
    <div className="search__input">
      <input
        {...props}
        ref={ref}
        className="input--search"
        placeholder="Search collection by contract address..."
        autoFocus
      />
    </div>
  );
});

export default SearchInput;
