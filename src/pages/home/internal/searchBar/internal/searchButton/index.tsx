import "./styles.css";

import * as ICONS from "@/constants/icons";
import Icon from "@/components/icon";

function SearchButton() {
  return (
    <div className="search__button">
      <div className="search__separator"></div>

      <button className="button--search" type="submit">
        <div className="icon-box">
          <Icon icon={ICONS.SEARCH} />
        </div>
      </button>
    </div>
  );
}

export default SearchButton;
