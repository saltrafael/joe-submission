import React from "react";

import "./styles.css";

import Spinner from "@/components/spinner";

import { useAppSelector } from "@/hooks/use-app-selector";
import { selectCollectionForAddress } from "@/redux/selectors/collections";

type Props = {
  address: string;
  didSubmit: boolean;
  isSubmitting: boolean;
  setIsSubmitting: (isSubmitting: boolean) => void;
};

function SearchActions(props: Props) {
  const { address, didSubmit, isSubmitting, setIsSubmitting } = props;

  const collection = useAppSelector((state) =>
    selectCollectionForAddress(state, address)
  );

  const loading = isSubmitting && collection === undefined;
  const loadFailed = didSubmit && collection === null;

  React.useEffect(() => {
    if (isSubmitting && collection !== undefined) {
      setIsSubmitting(false);
    }
  }, [isSubmitting, collection]);

  return (
    <>
      <Spinner active={loading} />

      {loadFailed && (
        <div className="error-message">
          <h3>Error</h3>
          <p>An error has occurred. Please try again.</p>
        </div>
      )}
    </>
  );
}

export default SearchActions;
