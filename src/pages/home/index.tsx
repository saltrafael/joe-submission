import SearchBar from "./internal/searchBar";

type Props = {
  address?: string;
};

function Home(props: Props) {
  const { address } = props;
  return <SearchBar address={address} />;
}

export default Home;
