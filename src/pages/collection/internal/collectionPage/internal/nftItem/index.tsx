import "./styles.css";
import { buildIpfsUrl } from "@/util/ipfs";

type Props = {
  item: any;
};

function NFTItem(props: Props) {
  const { item } = props;

  const { metadata } = item || {};
  const { name, image } = (metadata && JSON.parse(metadata)) || {};

  const itemSrc =
    image &&
    (image.startsWith("ipfs://")
      ? buildIpfsUrl(image.replace("ipfs://", ""))
      : image);

  return (
    <li className="nft-list__item">
      <div className="nft__wrapper">
        <div
          style={
            item === undefined ? {} : { backgroundImage: `url(${itemSrc})` }
          }
          className={item === undefined ? "nft__skeleton" : "nft__img"}
        />

        <h4 className="nft__name">{name}</h4>
      </div>
    </li>
  );
}

export default NFTItem;
