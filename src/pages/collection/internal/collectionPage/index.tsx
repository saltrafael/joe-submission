import React from "react";
import classNames from "classnames";

import "./styles.css";

import { buildIpfsUrl } from "@/util/ipfs";

import {
  selectCollectionForAddress,
  selectItemsForCollectionAddress,
} from "@/redux/selectors/collections";
import {
  doMoralisStart,
  doFetchItemsForCollection,
} from "@/redux/actions/collections";
import { useDispatch } from "react-redux";

import * as ICONS from "@/constants/icons";
import Icon from "@/components/icon";
import { useAppSelector } from "@/hooks/use-app-selector";

import NFTItem from "./internal/nftItem";

type Props = {
  address: string;
};

function CollectionPage(props: Props) {
  const { address } = props;

  const [collection, items]: [any, any] = [
    useAppSelector((state) => selectCollectionForAddress(state, address)),
    useAppSelector((state) => selectItemsForCollectionAddress(state, address)),
  ];
  const dispatch = useDispatch();

  const descriptionBox: any = React.useRef();

  const [showMore, setShowMore] = React.useState<boolean | null>();

  const {
    name,
    description,
    banner_image_url: bannerImg,
    image_url: profileImg,
    twitter_username: twitter,
    external_url: website,
    discord_url: discord,
  } = collection;

  React.useEffect(() => {
    dispatch(doMoralisStart() as unknown as any)
      .then(() =>
        dispatch(doFetchItemsForCollection({ address }) as unknown as any)
      )
      .catch(() => {});
  }, [address]);

  const descriptionBoxRef: any = React.useCallback((node: any) => {
    if (node) {
      descriptionBox.current = node;

      // Get the scrollHeight and offsetHeight of the element
      const scrollHeight = node.scrollHeight;
      // Get the offsetHeight and lineHeight of the element
      const offsetHeight = node.offsetHeight;

      const isOverflowing = scrollHeight > offsetHeight;
      const lineHeight = getComputedStyle(node).lineHeight;

      // Convert the lineHeight to a number (removing the "px" unit)
      const lineHeightNum = parseInt(lineHeight, 10);

      // Divide the offsetHeight by the lineHeight to get the number of lines
      const lineCount = Math.round(offsetHeight / lineHeightNum);

      if (!isOverflowing && lineCount <= 3) setShowMore(null);
      else setShowMore(false);
    }
  }, []);

  React.useEffect(() => {
    function updateSize() {
      if (descriptionBox.current) {
        descriptionBoxRef(descriptionBox.current);
      }
    }

    window.addEventListener("resize", updateSize);
    return () => window.removeEventListener("resize", updateSize);
  }, []);

  const nftList = items || Array.from(new Array(20));

  return (
    <div className="collection-page">
      <div className="collection-page__banner">
        <div
          className="banner__img"
          style={{ backgroundImage: `url(${bannerImg})` }}
        />

        <div className="collection-page__profile">
          <div
            className="profile__img"
            style={{ backgroundImage: `url(${profileImg})` }}
          />
        </div>
      </div>

      <div className="collection-page__content">
        <div className="content__header">
          <h2 className="header__title">{name}</h2>

          <div className="header__socials">
            {twitter && (
              <a target="_blank" href={`https://twitter.com/${twitter}`}>
                <Icon
                  icon={ICONS.TWITTER}
                  style={{ filter: "none" }}
                  stroke={"none"}
                  viewBox={"0 0 60 60"}
                  size={30}
                />
              </a>
            )}

            {discord && (
              <a target="_blank" href={`https://twitter.com/${twitter}`}>
                <Icon
                  icon={ICONS.DISCORD}
                  style={{ filter: "none" }}
                  stroke={"none"}
                  viewBox={"0 0 1024 1024"}
                  size={30}
                />
              </a>
            )}

            {website && (
              <a target="_blank" href={website}>
                <Icon icon={ICONS.GLOBE} size={30} />
              </a>
            )}
          </div>
        </div>

        <div className="content__description">
          <h5
            ref={descriptionBoxRef}
            className={classNames("description__txt", {
              active: showMore || showMore === null,
            })}
          >
            {description}
          </h5>

          {showMore !== null && (
            <button
              className="description__expand-btn"
              onClick={() => setShowMore((prev) => !prev)}
            >
              <Icon icon={showMore ? ICONS.COLLAPSE : ICONS.EXPAND} />
            </button>
          )}
        </div>

        <ul className="content__nft-list">
          {nftList.map((item: any, index: number) => (
            <NFTItem key={item ? item.token_id : index} item={item} />
          ))}
        </ul>
      </div>
    </div>
  );
}

export default CollectionPage;
