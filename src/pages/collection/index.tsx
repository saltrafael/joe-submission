import React from "react";

import { useParams } from "react-router-dom";

import Home from "@/pages/home";
import Spinner from "@/components/spinner";
import CollectionPage from "./internal/collectionPage";

import { selectCollectionForAddress } from "@/redux/selectors/collections";
import { doSearchCollection } from "@/redux/actions/collections";
import { useDispatch } from "react-redux";
import { useAppSelector } from "@/hooks/use-app-selector";

function Collection() {
  const { addr: address } = useParams();

  const dispatch = useDispatch();
  const collection =
    address &&
    useAppSelector((state) => selectCollectionForAddress(state, address));

  React.useEffect(() => {
    if (address) {
      dispatch(doSearchCollection({ address }) as unknown as any);
    }
  }, [address]);

  if (collection === undefined) {
    return <Spinner active={true} />;
  }

  if (collection === null || !address) {
    return <Home address={address} />;
  }

  return <CollectionPage address={address} />;
}

export default Collection;
