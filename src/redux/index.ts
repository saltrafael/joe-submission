import { configureStore } from "@reduxjs/toolkit";
import { collectionsReducer } from "@/redux/reducers/collections";

export const store = configureStore({
  reducer: {
    collections: collectionsReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
