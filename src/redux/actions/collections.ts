import Moralis from "moralis";

import { EvmChain } from "@moralisweb3/common-evm-utils";

import { RootState } from "@/redux";
import { actions } from "@/redux/reducers/collections";
import { createAsyncThunk } from "@reduxjs/toolkit";

import {
  selectMoralisStarting,
  selectIsSearchingAddress,
  selectHasCollectionForAddress,
  selectItemsFetchingForAddress,
} from "@/redux/selectors/collections";

export const doMoralisStart = createAsyncThunk(
  "collections/doMoralisStart",
  async (payload, { dispatch, getState }) => {
    const state: any = getState();
    const moralisStarting = selectMoralisStarting(state);

    if (moralisStarting) return Promise.resolve();

    dispatch(actions.moralisStart());

    return await Moralis.start({
      apiKey:
        "AMiClhchxqbV74H5aN7EtrWPxcf3PysuXVrS0ryN6eCOLDzZ7nXKswDllIce7hBF",
    });
  }
);

export const doFetchItemsForCollection = createAsyncThunk(
  "collections/doFetchItemsForCollection",
  async ({ address }: { address: string }, { dispatch, getState }) => {
    const state: any = getState();
    const itemsFetching = selectItemsFetchingForAddress(state, address);

    if (itemsFetching) return Promise.resolve();

    dispatch(actions.collectionItemsStart(address));

    const chain = EvmChain.ETHEREUM;

    try {
      const response = await Moralis.EvmApi.nft.getContractNFTs({
        address,
        chain,
        disableTotal: true,
        limit: 20,
      });

      return Promise.resolve({ address, items: response.toJSON().result });
    } catch (error: any) {
      return Promise.reject(error.message);
    }
  }
);

export const doSearchCollection = createAsyncThunk(
  "collections/doSearchCollection",
  async ({ address }: { address: string }, { dispatch, getState }) => {
    const state = getState() as RootState;
    const isSearchingAddress = selectIsSearchingAddress(state, address);
    const hasCollection = selectHasCollectionForAddress(state, address);

    if (isSearchingAddress || hasCollection) return Promise.resolve();

    dispatch(actions.collectionSearchStart(address));

    try {
      const options = { method: "GET" };
      const response = await fetch(
        `https://api.opensea.io/api/v1/asset_contract/${address}`,
        options
      )
        .then((response) => response.json())
        .then((response) => response);

      const {
        collection: { slug },
      } = response;

      const collectionResponse = await fetch(
        `https://api.opensea.io/api/v1/collection/${slug}`,
        options
      )
        .then((response) => response.json())
        .then((response) => response);

      return Promise.resolve({
        address,
        collectionResponse: collectionResponse.collection,
      });
    } catch (error: any) {
      return Promise.reject(error.message);
    }
  }
);
