import { createSlice } from "@reduxjs/toolkit";

import {
  doFetchItemsForCollection,
  doMoralisStart,
  doSearchCollection,
} from "@/redux/actions/collections";

interface CollectionsState {
  moralisStarting: boolean | undefined;
  moralisStarted: boolean | undefined;
  searchingAddresses: Array<string>;
  byContractAddress: { [contractAddress: string]: {} | null };
  byAddress: { [contractAddress: string]: string };
  bySlug: { [contractAddress: string]: string };
  itemsFetchingAddresses: Array<string>;
  itemsByCollection: { [address: string]: Array<any> | null };
}

const initialState: CollectionsState = {
  moralisStarting: undefined,
  moralisStarted: undefined,
  searchingAddresses: [],
  byContractAddress: {},
  byAddress: {},
  bySlug: {},
  itemsFetchingAddresses: [],
  itemsByCollection: {},
};

function removeSearchingAddress(
  state: CollectionsState,
  contractAddress: string
) {
  const newSearchingAddresses = new Set(state.searchingAddresses);
  if (newSearchingAddresses.has(contractAddress)) {
    newSearchingAddresses.delete(contractAddress);
  }

  return { searchingAddresses: Array.from(newSearchingAddresses) };
}

function removeItemsFetchingAddress(
  state: CollectionsState,
  contractAddress: string
) {
  const newItemsFetchingAddresses = new Set(state.itemsFetchingAddresses);
  if (newItemsFetchingAddresses.has(contractAddress)) {
    newItemsFetchingAddresses.delete(contractAddress);
  }

  return { searchingAddresses: Array.from(newItemsFetchingAddresses) };
}

export const collectionsSlice = createSlice({
  name: "collections",
  initialState,
  reducers: {
    collectionSearchStart: (state, action) => {
      const address = action.payload;

      const newSearchingAddresses = new Set(state.searchingAddresses);
      newSearchingAddresses.add(address);

      Object.assign(state, {
        searchingAddresses: Array.from(newSearchingAddresses),
      });
    },

    moralisStart: (state) => {
      Object.assign(state, { moralisStarting: true });
    },

    collectionItemsStart: (state, action) => {
      const address = action.payload;

      const newItemsFetchingAddresses = new Set(state.itemsFetchingAddresses);
      newItemsFetchingAddresses.add(address);

      Object.assign(state, {
        itemsFetchingAddresses: Array.from(newItemsFetchingAddresses),
      });
    },
  },
  extraReducers(builder) {
    builder.addCase(doSearchCollection.fulfilled, (state, action) => {
      if (!action.payload) return;

      const { address, collectionResponse } = action.payload;

      const collectionContractAddress =
        collectionResponse.primary_asset_contracts[0].address;

      const newByContractAddress = Object.assign({}, state.byContractAddress);
      newByContractAddress[collectionContractAddress] = collectionResponse;

      const newByAddress = Object.assign({}, state.byAddress);
      newByAddress[address] = collectionContractAddress;

      const newBySlug = Object.assign({}, state.bySlug);
      newBySlug[collectionResponse.slug] = collectionContractAddress;

      Object.assign(state, {
        byContractAddress: newByContractAddress,
        byAddress: newByAddress,
        bySlug: newBySlug,
        ...removeSearchingAddress(state, Object.keys(action.payload)[0]),
      });
    });
    builder.addCase(doSearchCollection.rejected, (state, action) => {
      const { address } = action.meta.arg;
      const newByContractAddress = Object.assign({}, state.byContractAddress);
      newByContractAddress[address] = null;

      Object.assign(state, {
        byContractAddress: newByContractAddress,
        ...removeSearchingAddress(state, address),
      });
    });

    builder.addCase(doMoralisStart.fulfilled, (state) => {
      Object.assign(state, { moralisStarted: true, moralisStarting: false });
    });
    builder.addCase(doMoralisStart.rejected, (state) => {
      Object.assign(state, { moralisStarting: false });
    });

    builder.addCase(doFetchItemsForCollection.fulfilled, (state, action) => {
      if (!action.payload) return;

      const { address, items } = action.payload;

      const newItemsByCollection = Object.assign({}, state.itemsByCollection);
      newItemsByCollection[address] = items as any;

      Object.assign(state, {
        itemsByCollection: newItemsByCollection,
        ...removeItemsFetchingAddress(state, address),
      });
    });
    builder.addCase(doFetchItemsForCollection.rejected, (state, action) => {
      const { address } = action.meta.arg;

      const newItemsByCollection = Object.assign({}, state.itemsByCollection);
      newItemsByCollection[address] = null;

      Object.assign(state, {
        itemsByCollection: newItemsByCollection,
        ...removeItemsFetchingAddress(state, address),
      });
    });
  },
});

export const { actions, reducer: collectionsReducer } = collectionsSlice;
