import { RootState } from "@/redux";

export const selectState = (state: RootState) => state.collections || {};

export const selectMoralisStarting = (state: RootState) =>
  selectState(state).moralisStarting;
export const selectMoralisStarted = (state: RootState) =>
  selectState(state).moralisStarted;

export const selectSearchingAddresses = (state: RootState) =>
  selectState(state).searchingAddresses;
export const selectIsSearchingAddress = (state: RootState, address: string) =>
  selectSearchingAddresses(state).includes(address);

export const selectByContractAddress = (state: RootState) =>
  selectState(state).byContractAddress;

export const selectCollectionForAddress = (state: RootState, address: string) =>
  selectByContractAddress(state)[address];

export const selectHasCollectionForAddress = (
  state: RootState,
  address: string
) => !!selectCollectionForAddress(state, address);

export const selectItemsFetchingAddresses = (state: RootState) =>
  selectState(state).itemsFetchingAddresses;
export const selectItemsFetchingForAddress = (
  state: RootState,
  address: string
) => selectItemsFetchingAddresses(state).includes(address);

export const selectItemsByCollection = (state: RootState) =>
  selectState(state).itemsByCollection;
export const selectItemsForCollectionAddress = (
  state: RootState,
  address: string
) => selectItemsByCollection(state)[address];
