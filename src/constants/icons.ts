export const SEARCH = "Search";
export const EXPAND = "Expand";
export const COLLAPSE = "Collapse";
export const TWITTER = "Twitter";
export const DISCORD = "Discord";
export const GLOBE = "Globe";
