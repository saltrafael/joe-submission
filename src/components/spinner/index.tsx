import "./styles.css";

import classNames from "classnames";

type Props = {
  active?: boolean;
};

function Spinner(props: Props) {
  const { active } = props;
  return <div className={classNames("spinner", { active })}></div>;
}

export default Spinner;
