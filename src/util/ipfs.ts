import * as IPFS from "@/constants/ipfs";

export function buildIpfsUrl(hash: string): string {
  return `${IPFS.GATEWAY}${hash}`;
}
